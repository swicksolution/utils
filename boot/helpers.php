<?php

use Augeo\Util\Debug\Dumper;

if (! function_exists('die_print')) {
    /**
     * print_r all arguments and die
     */
    function die_print()
    {
        echo '<pre>';
        array_map(function ($x) {
            var_export($x);
            echo '<hr>';
        },
            func_get_args());
        echo '</pre>';
        die(1);
    }
}

if (! function_exists('die_dump')) {
    /**
     * var_dump all arguments and die
     */
    function die_dump()
    {
        echo '<pre>';
        array_map(function ($x) {
            var_dump($x);
            echo '<hr>';
        },
            func_get_args());
        echo '</pre>';
        die(1);
    }
}

if (! function_exists('die_pretty')) {
    /**
     * Symphony VarDumper all arguments and die
     */
    function die_pretty()
    {
        array_map(function ($x) {
            (new Dumper)->dump($x);
            echo '<hr>';
        },
            func_get_args());
        die(1);
    }
}

if (! function_exists('dp')) {
    /**
     * Symphony VarDumper all arguments
     */
    function dp()
    {
        call_user_func_array('die_pretty', func_get_args());
    }
}

if (! function_exists('print_pretty')) {
    /**
     * Symphony VarDumper all arguments
     */
    function print_pretty()
    {
        array_map(function ($x) {
            (new Dumper)->dump($x);
        },
            func_get_args());
    }
}

if (! function_exists('pp')) {
    /**
     * Symphony VarDumper all arguments
     */
    function pp()
    {
        call_user_func_array('print_pretty', func_get_args());
    }
}

if (! function_exists('array_has_keys')) {
    /**
     * array_key_exists for multiple keys
     *
     * @param array $array
     * @param array $keys
     * @return bool
     */
    function array_has_keys(array $array, $keys = [])
    {
        foreach ($keys as $key) {
            if (! array_key_exists($key, $array)) return false;
        }

        return true;
    }
}

if (! function_exists('array_alter_keys')) {
    /**
     * Apply a callback to each array key
     *
     * @param array          $array
     * @param string|Closure $cb
     * @return array
     */
    function array_alter_keys(array $array, $cb)
    {
        $cleaned = [];

        foreach ($array as $k => $v) {
            $key = is_callable($cb) ? call_user_func($cb, $k) : $k;
            $cleaned[$key] = is_array($v) ? array_alter_keys($v, $cb) : $v;
        }

        return $cleaned;
    }
}

if (! function_exists('array_get_not_empty')) {
    /**
     * array_get with empty check
     *
     * @param array  $array
     * @param string $key
     * @param null   $default
     * @return bool
     */
    function array_get_not_empty(array $array, $key, $default = null)
    {
        $get = array_get($array, $key, $default);

        return empty($get) ? $default : $get;
    }
}

if (! function_exists('array_has_not_empty')) {
    /**
     * array_has with empty check
     *
     * @param array  $array
     * @param string $key
     * @return bool
     */
    function array_has_not_empty(array $array, $key)
    {
        $get = array_get($array, $key, '');

        return ! empty($get);
    }
}

if (! function_exists('array_recursive_except')) {
    /**
     * Get all of the given array except for a specified array of items, recursively.
     *
     * @param  array        $array
     * @param  array|string $keys
     * @return array
     */
    function array_recursive_except($array, $keys)
    {
        // Get rid of bad first
        $cleaned = array_except($array, $keys);

        // If child arrays, handle those
        foreach ($cleaned as $k => $v) {
            if (is_array($v)) {
                $cleaned[$k] = array_recursive_except($v, $keys);
            }
        }

        return $cleaned;
    }
}

if (! function_exists('array_recursive_set')) {
    /**
     * Recursively set key to new value on a collection of assoc arrays
     *
     * Closure signature: ($array, $index, $key, $collection)
     *
     * @param  array         $collection
     * @param  string        $key
     * @param  mixed|Closure $value
     * @return array
     */
    function array_recursive_set($collection, $key, $value)
    {
        array_walk($collection,
            function (&$array, $index) use ($key, $value, $collection) {
                $array[$key] = $value instanceof Closure ?
                    call_user_func($value, $array, $index, $key, $collection) :
                    $value;
            });

        return $collection;
    }
}

if (! function_exists('array_multi_merge')) {
    /**
     * Merge 2 level deep assoc array into single assoc array
     *
     * @param  array $array
     * @return array
     */
    function array_multi_merge($array)
    {
        $cleaned = [];
        foreach ($array as $sub) {
            $cleaned = array_merge($cleaned, $sub);
        }

        return $cleaned;
    }
}

if (! function_exists('array_dot_assoc')) {
    /**
     * Flatten a multi-dimensional associative array with dots, leaving flat array values
     *
     * @param  array  $array
     * @param  string $prepend
     * @return array
     */
    function array_dot_assoc($array, $prepend = '')
    {
        $results = [];

        foreach ($array as $key => $value) {
            if (is_array($value) && array_is_assoc($value)) {
                $results = array_merge($results,
                    array_dot_assoc($value, $prepend . $key . '.'));
            }
            else {
                $results[$prepend . $key] = $value;
            }
        }

        return $results;
    }
}

if (! function_exists('array_undot')) {
    /**
     * Un-dot a dot-syntax array
     *
     * @param  array $array
     * @return array
     */
    function array_undot(array $array)
    {
        $out = [];

        foreach ($array as $key => $value) {
            array_set($out, $key, $value);
        }

        return $out;
    }
}

if (! function_exists('array_is_assoc')) {
    /**
     * Is array assoc
     *
     * @param  array $array
     * @return bool
     */
    function array_is_assoc($array)
    {
        $array = array_keys($array);

        return $array !== array_keys($array);
    }
}

if (! function_exists('array_deep_diff')) {
    /**
     * Perform deep diff against arrays
     *
     * @param $array1
     * @param $array2
     * @return array
     */
    function array_deep_diff($array1, $array2)
    {
        $dot1 = array_dot($array1);
        $dot2 = array_dot($array2);

        $diff = array_diff_assoc($dot1, $dot2);

        $out = [];

        foreach ($diff as $k => $v) {
            array_set($out, $k, $v);
        }

        return $out;
    }
}

if (! function_exists('array_sym_diff')) {
    /**
     * Perform symmetrical array diff
     *
     * @param array $array1
     * @param array $array2
     * @return array
     */
    function array_sym_diff(array $array1, array $array2)
    {
        return array_merge(array_diff($array1, $array2), array_diff($array2, $array1));
    }
}

if (! function_exists('safe_json_decode')) {
    /**
     * Safe decode JSON string
     *
     * If decode fails, return original string
     *
     * @param string $input
     * @return mixed
     */
    function safe_json_decode($input)
    {
        $original = $input;

        // Only strings
        if (! is_string($input)) return $original;

        // We have a string, trim it
        $input = trim($input);

        // empty returned as null from json_decode
        if (empty($input)) return $original;

        // valid json string for array/objects
        if (! preg_match('/^[\[\{]/', $input)) return $original;

        $json = json_decode($input, true);

        if (json_last_error() === JSON_ERROR_NONE) {
            return $json;
        }

        return $original;
    }
}

if (! function_exists('array_diff_changes')) {
    /**
     * Compares two arrays, and returns the diff and changeset
     *
     * The diff returned is an array of values from 'new' not in 'original'.
     * The changeset is an array that contains a record of the changes.
     *
     * @param array $original Original data
     * @param array $new      New data
     * @return array Results: [$diff, $changeset]
     */
    function array_diff_changes(array $original, array $new)
    {
        $original = array_dot_assoc($original);
        $new = array_dot_assoc($new);

        $diff = [];
        $changes = [];

        $logChange = function ($k, $v) use (&$diff, &$changes, $original) {
            array_set($diff, $k, $v);
            array_set($changes, $k, [array_get($original, $k, ''), $v]);
        };

        foreach ($new as $k => $v) {
            // Check for JSON encoded data
            $v = safe_json_decode($v);

            if (array_key_exists($k, $original)) {
                if (is_array($v) && is_array($original[$k])) {
                    if (empty($v)) {
                        $logChange($k, $v);
                    }
                    else {
                        $subDiff = array_sym_diff($v, $original[$k]);
                        if (! empty($subDiff)) {
                            $logChange($k, $v);
                        }
                    }
                }
                else {
                    // We could check for empty:
                    // if (empty($v) && empty($original[$k])) continue;
                    // But we want empty to empty in diff for now so we can remove empty attributes
                    // from DB.

                    if ($v != $original[$k]) {
                        $logChange($k, $v);
                    }
                }
            }
            else {
                ! empty($v) && $logChange($k, $v);
            }
        }

        return [$diff, $changes, $original, $new];
    }
}

if (! function_exists('array_collapse_by_keys')) {
    /**
     * Collapses assoc array of ['name' => 'flat array'] into a collection grouped by name
     *
     * Example input:
     *
     * [
     *   'foo' => [1, 2],
     *   'bar' => ['a', 'b'],
     * ]
     *
     * Output:
     *
     * [
     *   [
     *     'foo' => 1,
     *     'bar' => 'a',
     *   ],
     *   [
     *     'foo' => 2,
     *     'bar' => 'b',
     *   ],
     * ]
     *
     * It accepts a closure parameter to run on each parent row, that must return an array of
     * [key_name, key_array].
     *
     * @param array   $data
     * @param mixed   $collapseKey Group name or array of group name
     * @param Closure $callback    Callback that runs on each key => array, returning [$key, $array]
     * @return array
     */
    function array_collapse_by_keys(array $data, $collapseKey, Closure $callback = null)
    {
        switch (gettype($collapseKey)) {
            case 'array':
                foreach ($collapseKey as $groupName => $fields) {
                    $assoc = array_is_assoc($fields);

                    foreach ($fields as $key => $name) {
                        if (! $assoc) $key = $name;
                        if (array_key_exists($key, $data)) {
                            foreach ($data[$key] as $i => $value) {
                                $data[$groupName][$i][$name] = $value;
                            }
                        }
                    }

                    $assoc ?
                        array_forget($data, array_keys($fields)) :
                        array_forget($data, $fields);
                }
                break;
            case 'string':
                foreach ($data as $key => $v) {
                    if (is_array($v) && ! array_is_assoc($v)) {
                        foreach ($v as $i => $value) {
                            $data[$collapseKey][$i][$key] = $value;
                        }
                        array_forget($data, $key);
                    }
                }
                break;
        }

        return $data;
    }
}

if (! function_exists('array_collapse_by_values')) {
    /**
     *
     * @param array $data
     * @return array
     */
    function array_collapse_by_values(array $data)
    {
        $values = array_values($data);
        $keys = array_keys($data);
        $out = [];

        foreach ($values as $key => $val) {
            foreach (array_flatten((array)$val) as $v) {
                $out[(string)$v] = $keys[$key];
            }
        }

        return $out;
    }
}

if (! function_exists('str_from')) {
    /**
     * Return rest of string from last occurrence of $match:
     *
     * str_from('test%after', '%') == 'after'
     * str_from('test%after', '$') == 'test%after'
     * str_from('abcdef', 'abc') == 'def'
     * str_from('abcd', 'd') == ''
     *
     * @param string $string string
     * @param string $match  character(s) to match
     * @return string
     */
    function str_from($string, $match)
    {
        $index = strrpos($string, $match);

        return (string)substr($string, ($index !== false ? $index + strlen($match) : 0));
    }
}

if (! function_exists('array_multi_set')) {
    /**
     * Set all keys to value
     *
     * @param array $array
     * @param array $keys
     * @param mixed $value
     * @return string
     */
    function array_multi_set(array $array, array $keys, $value)
    {
        foreach ($keys as $k) array_has($array, $k) && array_set($array, $k, $value);

        return $array;
    }
}

if (! function_exists('hex_random')) {
    /**
     * Return random hex string of given length
     *
     * @param int $length
     * @return string
     */
    function hex_random($length = 16)
    {
        return substr(bin2hex(random_bytes(ceil($length / 2))), 0, $length);
    }
}

if (! function_exists('urlsafe_base64_encode')) {
    /**
     * Encode a string with URL-safe Base64.
     *
     * @param string $input The string you want encoded
     * @return string The base64 encode of what you passed in
     */
    function urlsafe_base64_encode($input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }
}

if (! function_exists('urlsafe_base64_decode')) {
    /**
     * Decode a string with URL-safe Base64.
     *
     * @param string $input A Base64 encoded string
     * @return string A decoded string
     */
    function urlsafe_base64_decode($input)
    {
        if ($remainder = strlen($input) % 4) {
            $input .= str_repeat('=', 4 - $remainder);
        }

        return base64_decode(strtr($input, '-_', '+/'));
    }
}

if (! function_exists('clean_explode')) {
    /**
     * Explode by regex pattern and filter out empty values.
     *
     * @param string $string
     * @param string $pattern Regex pattern (delimiter is '/')
     * @return array
     */
    function clean_explode($string, $pattern = '\W+')
    {
        return array_filter(
            preg_split("/{$pattern}/", (string)$string),
            function ($piece) { return ! empty($piece); }
        );
    }
}

if (! function_exists('array_only_values')) {
    /**
     * Filter array values
     *
     * @param array $array Flat array
     * @param array $values
     * @return array
     */
    function array_only_values(array $array, $values = [])
    {
        return empty($values) ? $array : array_values(array_intersect($array, (array)$values));
    }
}

if (! function_exists('deep_empty')) {
    /**
     * Checks if nested array is empty
     *
     * Assoc arrays are considered empty if values are empty.
     *
     * @param array $array
     * @return array
     */
    function deep_empty(array $array)
    {
        foreach ((array)$array as $k => $v) {
            if (is_array($v)) return deep_empty($v);
            if (! empty($v)) return false;
        }

        return true;
    }
}

if (! function_exists('str_unsnake')) {
    /**
     * Unsnake string
     *
     * @param      $string
     * @param bool $capitalize
     * @return string
     */
    function str_unsnake($string, $capitalize = true)
    {
        return $capitalize ?
            ucwords(str_replace(['-', '_'], ' ', $string)) :
            str_replace(['-', '_'], ' ', $string);
    }
}