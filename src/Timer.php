<?php

namespace Augeo\Util;

use Illuminate\Support\Collection;

/**
 * Class TimerService
 *
 * Service provider bound to 'timer', so accessed via app('timer')->method().
 *
 * Two ways to track time:
 *
 * 1. Call start(name) and stop(name). This method allows for nested results, meaning records do
 * not need to be stopped in the same order they were started. This is ideal for tracking events
 * that are non-linear.
 *
 * start(a);
 *   // Other method called
 *   start(b);
 *   // Something happens
 *   stop(b);
 * stop(a);
 * getLog();
 *
 * 2. Simple linear log with mark(name). This allows you to mark time without having to call
 * stop(). Ideal for tracking events in single scripts.
 *
 * mark(a);
 * // do something
 * mark(b);
 * // do something else
 * getLog();
 *
 * @package Augeo\Util
 */
class Timer
{
    private $log = [];
    private $formattedLog = null;
    private $mark = null;
    private $running = [];

    /**
     * @var int
     */
    private $requestStart;

    public function __construct($requestStart)
    {
        $this->requestStart = $requestStart;
    }
    
    /**
     * Start time record by name
     *
     * @param $name
     */
    public function start($name)
    {
        $this->running[$name] = microtime(true);
    }

    /**
     * Stop time record by name
     *
     * @param $name
     */
    public function stop($name)
    {
        if (array_key_exists($name, $this->running)) {
            $this->log[] = $this->calcTimes($name, $this->running[$name]);
            unset($this->running[$name]);
        }
    }

    /**
     * Add time mark
     *
     * @param $name
     */
    public function mark($name)
    {
        if ($this->mark === null) {
            $this->start($name);
        }
        else {
            $this->stop($this->mark);
            $this->start($name);
        }
        $this->mark = $name;
    }

    /**
     * Get time log as assoc array
     *
     * Stops all open trackers.
     *
     * @return array
     */
    public function getLog()
    {
        // Stop open trackers
        if ($this->mark !== null) $this->stop($this->mark);
        foreach (array_keys($this->running) as $name) $this->stop($name);

        $log = (new Collection($this->log))->sortBy('start');

        $first = $log->first();

        $log = $log->prepend([
            'name'     => 'app_boot',
            'start'    => 0,
            'end'      => 1,
            'duration' => floatval($first['elapsed']) - floatval($first['duration']),
            'elapsed'  => 0,

        ]);

        $clean = function ($e) {
            return [
                'd' => number_format($e['duration'], 4, '.', ''),
                'e' => number_format($e['elapsed'], 4, '.', ''),
            ];
        };

        $processed = [];
        $events = [];
        $log->each(function ($e) use (&$processed, &$events, $clean) {
            $sub = false;
            $tree = [];
            if (! empty($processed)) {
                for ($i = 0, $j = count($processed); $i < $j; $i++) {
                    if ($e['start'] < $processed[$i]['end']) {
                        $tree[] = $processed[$i]['name'];
                    }
                }
                if (! empty($tree)) {
                    $tree[] = $e['name'];
                    array_set($events, implode('.sub.', $tree), $clean($e));
                    $sub = true;
                }
            }
            if (! $sub) {
                $events[$e['name']] = $clean($e);
            }
            array_push($processed, $e);
        });

        $this->formattedLog = [
            'total_elapsed' => microtime(true) - $this->requestStart,
            'events'        => $events,
        ];

        return $this->formattedLog;
    }

    /**
     * Get time log as flat array
     *
     * Assoc arrays (JSON objects) are sometimes sorted by key:
     * http://stackoverflow.com/q/5020699/409179
     *
     * This will return the data as flat arrays that will maintain sort order.
     *
     * @return array
     */
    public function getFlatLog()
    {
        $logs = $this->formattedLog ?: $this->getLog();

        return $this->collapseArray($logs['events']);
    }

    private function calcTimes($name, $start)
    {
        $end = microtime(true);

        return [
            'name'     => $name,
            'start'    => $start,
            'end'      => $end,
            'duration' => abs($end - $start),
            'elapsed'  => abs($end - $this->requestStart),
        ];
    }

    private function collapseArray($input)
    {
        $output = [];
        foreach ($input as $name => $value) {
            $a = array_merge(['n' => $name], $value);
            if (array_key_exists('sub', $value)) {
                $a['s'] = $this->collapseArray($value['sub']);
            }
            $output[] = $a;
        }

        return $output;
    }
}